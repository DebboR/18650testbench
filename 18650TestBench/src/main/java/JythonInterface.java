import org.python.core.PyInteger;
import org.python.util.PythonInterpreter;

public class JythonInterface {
    private static IHardware hardware;
    private static AlgorithmForm algorithmForm;


    // Initialization methods //
    public static void setHardware(IHardware h){
        hardware = h;
    }

    public static void setAlgorithmForm(AlgorithmForm af){
        algorithmForm = af;
    }

    // Algorithm Interface Methods //
    public static void done(){
        if(algorithmForm != null)
            algorithmForm.algorithmDone();
    }

    public static void runSubAlgorithm(String path){
        // Runs another algorithm until it is finished before continuing
        algorithmForm.runSubAlgorithm(path);
    }

    // Hardware Interface Methods //
    public static boolean getCellCharging(int cellNumber){
        return hardware.getCellCharging(cellNumber);
    }
    public static void setCellCharging(int cellNumber, boolean charging){
        hardware.setCellCharging(cellNumber, charging);
    }

    public static boolean getLoadConnected(){
        return hardware.getLoadConnected();
    }
    public static void setLoadConnected(boolean connected){
        hardware.setLoadConnected(connected);
    }

    public static boolean getChargeConnected(){
        return hardware.getChargeConnected();
    }
    public static void setChargeConnected(boolean connected){
        hardware.setChargeConnected(connected);
    }

    public static double getCellVoltage(int cellNumber){
        return hardware.getCellVoltage(cellNumber);
    }
    public static double getCellCurrent(int cellNumber){
        return hardware.getCellCurrent(cellNumber);
    }
}
