import com.pi4j.io.gpio.*;
import com.pi4j.io.i2c.I2CBus;
import com.pi4j.io.i2c.I2CDevice;
import com.pi4j.io.i2c.I2CFactory;

public class TestBenchHardwareV2 extends IHardware {
    private static final int I2CBUS = I2CBus.BUS_1;
    private static final byte PCF8574_ADDR = 0x25;
    private static final byte[] PCF8574_RELAY_PINS = {0, 1, 2, 3, 4, 5, 6, 7};
    private static final byte[] CELL_INA230_ADDRS = {0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47};
    // Not sure which yet...
    private static final byte LOAD_INA230_ADDR = 0x48;
    private static final byte CHARGE_INA230_ADDR = 0x49;
    private static final Pin LOAD_RELAY_GPIO_PIN = RaspiPin.GPIO_07;
    private static final Pin CHARGE_RELAY_GPIO_PIN = RaspiPin.GPIO_01;

    private static final int INA_230_CONFIG_ADDR = 0x00;
    private static final int INA_230_SHUNT_VOLTAGE_ADDR = 0x01;
    private static final int INA_230_BUS_VOLTAGE_ADDR = 0x02;
    private static final double INA_230_BUS_VOLTAGE_LSB = 0.00125;
    private static final double INA_230_SHUNT_VOLTAGE_LSB = 0.0000025;

    // 64 averages, 2ms conversion time, continuous mode
    private static final byte[] INA_230_CONFIG = {(byte) 0b01000111, (byte) 0b00100111};

    private static final double SENSE_RESISTOR = 0.003;

    private ILogHandler logHandler;
    private I2CDevice PCF8574;
    private I2CDevice loadINA230;
    private I2CDevice chargeINA230;
    private I2CDevice[] cellINA230S = new I2CDevice[IHardware.NUMBER_OF_CELLS];
    private boolean[] cellINA230Online = new boolean[IHardware.NUMBER_OF_CELLS];
    private GpioPinDigitalOutput loadRelayPin;
    private GpioPinDigitalOutput chargeRelayPin;

    private byte channelRelayState = (byte) 0x00;
    private boolean loadConnected = false;
    private boolean chargeConnected = false;

    @Override
    public void init(ILogHandler logHandler) {
        this.logHandler = logHandler;

        try {
            // Get I²C bus
            I2CBus i2cBus = I2CFactory.getInstance(I2CBUS);

            // Define all I²C devices
            this.PCF8574 = i2cBus.getDevice(PCF8574_ADDR);
            this.loadINA230 = i2cBus.getDevice(LOAD_INA230_ADDR);
            this.chargeINA230 = i2cBus.getDevice(CHARGE_INA230_ADDR);
            for(int i=0; i<IHardware.NUMBER_OF_CELLS; i++)
                cellINA230S[i] = i2cBus.getDevice(CELL_INA230_ADDRS[i]);

            // Configure load and charge channels
            this.configureINA230(this.loadINA230);
            this.configureINA230(this.chargeINA230);

            // Wait for initialization
            Thread.sleep(1500);

            // Set all charge relays to known state
            PCF8574.write(channelRelayState);

            // Get GPIO controller
            GpioController gpioController = GpioFactory.getInstance();

            // Get Load Relay pin
            loadRelayPin = gpioController.provisionDigitalOutputPin(LOAD_RELAY_GPIO_PIN);

            // Set Load Relay to known state
            loadRelayPin.setState(loadConnected);

            // Get Charge Relay pin
            chargeRelayPin = gpioController.provisionDigitalOutputPin(CHARGE_RELAY_GPIO_PIN);

            // Set Charge Relay to known state
            chargeRelayPin.setState(chargeConnected);

        } catch (Exception e) {
            this.logHandler.addLog(new Log("Exception", "Something went wrong while initializing TestBenchHardware"));
            e.printStackTrace();
        }
    }

    @Override
    protected double measureCellVoltage(int cellNumber) {
        try {
            double voltage = this.readINA230Voltage(this.cellINA230S[cellNumber]);

            // Chip is online!
            if(!this.cellINA230Online[cellNumber]){
                this.initCellINA230(cellNumber);
                return 0;
            }
            return voltage;
        } catch (Exception e){
            // No reaction of chip: OFFLINE
            this.cellINA230Online[cellNumber] = false;
            return 0;
        }
    }

    @Override
    protected double measureCellCurrent(int cellNumber) {
        try {
            if(this.cellINA230Online[cellNumber]) {
                return this.readINA230Current(this.cellINA230S[cellNumber]);
            }
        } catch (Exception e){
            // No reaction of chip: OFFLINE
            this.cellINA230Online[cellNumber] = false;
        }
        return 0;
    }

    @Override
    public boolean getCellCharging(int cellNumber) {
        return ((this.channelRelayState & (1 << PCF8574_RELAY_PINS[cellNumber])) != 0);
    }

    @Override
    public void setCellCharging(int cellNumber, boolean charging) {
        if(charging != this.getCellCharging(cellNumber)){
            this.channelRelayState &= ~(1 << PCF8574_RELAY_PINS[cellNumber]);
            this.channelRelayState |= ((charging ? 1 : 0) << PCF8574_RELAY_PINS[cellNumber]);

            try {
                this.PCF8574.write(this.channelRelayState);
            } catch (Exception e){
                this.logHandler.addLog(new Log("Exception", "Something went wrong while setting cell charging on TestBenchHardware"));
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean getLoadConnected() {
        return this.loadConnected;
    }

    @Override
    public void setLoadConnected(boolean connected) {
        this.loadConnected = connected;
        this.loadRelayPin.setState(this.loadConnected);
    }

    @Override
    public boolean getChargeConnected() {
        return this.chargeConnected;
    }

    @Override
    public void setChargeConnected(boolean connected) {
        this.chargeConnected = connected;
        this.chargeRelayPin.setState(this.chargeConnected);
    }

    @Override
    public double measureLoadVoltage() {
        try {
            return this.readINA230Voltage(this.loadINA230);
        } catch (Exception e){}
        return 0;
    }

    @Override
    public double measureLoadCurrent() {
        try {
            return this.readINA230Current(this.loadINA230);
        } catch (Exception e) {}
        return 0;
    }

    @Override
    public double measureChargeVoltage() {
        try {
            return this.readINA230Voltage(this.chargeINA230);
        } catch (Exception e) {}
        return 0;
    }

    @Override
    public double measureChargeCurrent() {
        try {
            return this.readINA230Current(this.chargeINA230);
        } catch (Exception e) {}
        return 0;
    }

    // Initialize config and calibration of INA220
    private void initCellINA230(int cellNumber){
        try{
            // Write config
            this.configureINA230(this.cellINA230S[cellNumber]);

            // Set as online
            this.cellINA230Online[cellNumber] = true;

            this.logHandler.addLog(new Log("TestBench", "Initialized INA230 of cell " + cellNumber));
        } catch(Exception e){}
    }

    private void configureINA230(I2CDevice ina) throws Exception{
        // Write config
        ina.write(INA_230_CONFIG_ADDR, INA_230_CONFIG, 0, 2);
    }

    private double readINA230Voltage(I2CDevice ina) throws Exception{
        // Read bus voltage register
        byte[] buffer = {0x00, 0x00};
        ina.read(INA_230_BUS_VOLTAGE_ADDR, buffer, 0, 2);

        // Convert to voltage
        int value = (0xFF00 & ((int) buffer[0] << 8)) | (0xFF & ((int) buffer[1]));
        return (INA_230_BUS_VOLTAGE_LSB * (double) value);
    }

    private double readINA230Current(I2CDevice ina) throws Exception{
        // Read shunt voltage register
        byte[] buffer = {0x00, 0x00};
        ina.read(INA_230_SHUNT_VOLTAGE_ADDR, buffer, 0, 2);

        // Convert to current
        short value = (short) (((short) 0xFF00 & ((short) buffer[0] << 8)) | ((short) 0xFF & ((short) buffer[1])));
        return (((double) value * INA_230_SHUNT_VOLTAGE_LSB) / SENSE_RESISTOR);
    }
}
