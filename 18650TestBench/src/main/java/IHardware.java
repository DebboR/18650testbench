import java.util.ArrayList;

public abstract class IHardware {
    // Constants
    public static int NUMBER_OF_CELLS = 8;

    // Global variables
    protected double[] cellVoltages = new double[NUMBER_OF_CELLS];
    protected double[] cellCurrents = new double[NUMBER_OF_CELLS];
    protected double loadVoltage = 0;
    protected double loadCurrent = 0;
    protected double chargeVoltage = 0;
    protected double chargeCurrent = 0;

    // Abstract methods to be implemented
    public abstract void init(ILogHandler logHandler);

    protected abstract double measureCellVoltage(int cellNumber);
    protected abstract double measureCellCurrent(int cellNumber);

    public abstract boolean getCellCharging(int cellNumber);
    public abstract void setCellCharging(int cellNumber, boolean charging);

    public abstract boolean getLoadConnected();
    public abstract void setLoadConnected(boolean connected);

    public abstract boolean getChargeConnected();
    public abstract void setChargeConnected(boolean connected);

    // Helper functions
    public void performMeasurements(){
        for(int i=0; i<NUMBER_OF_CELLS; i++){
            cellVoltages[i] = this.measureCellVoltage(i);
            cellCurrents[i] = this.measureCellCurrent(i);
        }
        loadVoltage = this.measureLoadVoltage();
        loadCurrent = this.measureLoadCurrent();
        chargeVoltage = this.measureChargeVoltage();
        chargeCurrent = this.measureChargeCurrent();
    }

    public double getCellVoltage(int cellNumber){
        return cellVoltages[cellNumber];
    }

    public double getCellCurrent(int cellNumber){
        return cellCurrents[cellNumber];
    }

    public double getLoadVoltage(){
        return loadVoltage;
    }

    public double getLoadCurrent(){
        return loadCurrent;
    }

    public double getChargeVoltage(){
        return chargeVoltage;
    }

    public double getChargeCurrent(){
        return chargeCurrent;
    }

    public double measureLoadVoltage(){
        double total = 0;
        for(int i=0; i<NUMBER_OF_CELLS; i++)
            total += this.getCellVoltage(i);
        return total;
    }

    public double measureLoadCurrent(){
        double total = 0;
        for(int i=0; i<NUMBER_OF_CELLS; i++){
            if(getCellCharging(i))
                return 0;
            total += getCellCurrent(i);
        }
        return total / NUMBER_OF_CELLS;
    }

    public abstract double measureChargeVoltage();

    public double measureChargeCurrent(){
        double total = 0;
        for(int i=0; i<NUMBER_OF_CELLS; i++){
            if(getCellCharging(i))
                total += getCellCurrent(i);
        }
        return total;
    }

    public void setHardwareSafe(){
        this.setLoadConnected(false);
        this.setChargeConnected(false);
        for(int i=0; i<NUMBER_OF_CELLS; i++)
            this.setCellCharging(i, false);
    }
}
