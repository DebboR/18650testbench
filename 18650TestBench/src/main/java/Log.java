import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Log {
    public Date date;
    public String type;
    public String message;

    private static DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    public Log(String type, String message) {
        this.date = new Date();
        this.type = type;
        this.message = message;
    }

    @Override
    public String toString() {
        return "[" + dateFormat.format(this.date) + "][" + this.type + "] " + this.message;
    }
}
