import org.python.util.PythonInterpreter;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.util.ArrayList;

public class AlgorithmForm {
    private JPanel panel1;
    private JButton runButton;
    private JButton stopButton;
    private JLabel fileNameLabel;
    private JButton fileSelectButton;

    private ILogHandler logHandler;
    private IHardware hardware;
    private JFileChooser fileChooser;

    private File algorithmFile;

    private ArrayList<PythonInterpreter> pythonInterpreters = new ArrayList<>();
    private Boolean paused = false;

    public AlgorithmForm() {
        fileChooser = new JFileChooser();

        fileSelectButton.addActionListener(e -> {
            int returnValue = fileChooser.showOpenDialog(panel1);

            if (returnValue == JFileChooser.APPROVE_OPTION) {
                algorithmFile = fileChooser.getSelectedFile();
                logHandler.addLog(new Log("Algorithm", "Selected file: " + algorithmFile.getPath()));
                fileNameLabel.setText(algorithmFile.getName());

                pythonInterpreters.clear();
                paused = false;
                runButton.setText("Run");
            }
        });

        runButton.addActionListener(e -> {
            if (algorithmFile != null) {
                try {
                    if (pythonInterpreters.size() == 0 && !paused) {
                        addPythonInterpreterToStack(algorithmFile);

                        runButton.setText("Pause");
                    } else if (!paused) {
                        paused = true;
                        runButton.setText("Run");
                    } else if (paused) {
                        paused = false;
                        runButton.setText("Pause");
                    }
                } catch (Exception ex) {
                    logHandler.addLog(new Log("Algorithm", "Error while opening algorithmFile"));
                    ex.printStackTrace();

                    this.hardware.setHardwareSafe();
                }
            }
        });

        stopButton.addActionListener(e -> {
            if (algorithmFile != null) {
                try {
                    while (pythonInterpreters.size() > 0) {
                        pythonInterpreters.get(0).exec("done()");
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    this.hardware.setHardwareSafe();
                }
                pythonInterpreters.clear();
                paused = false;
                runButton.setText("Run");
            }
        });

        JythonInterface.setAlgorithmForm(this);
    }

    private void addPythonInterpreterToStack(File algorithmFile) throws FileNotFoundException {
        pythonInterpreters.add(new PythonInterpreter());
        pythonInterpreters.get(pythonInterpreters.size() - 1).setOut(new OutputStream() {
            private String output = "";

            @Override
            public void write(int b) throws IOException {
                byte[] bytes = new byte[1];
                bytes[0] = (byte) (b & 0xff);
                if (b != '\n' && b != '\r')
                    output = output + new String(bytes);
            }

            @Override
            public void flush() throws IOException {
                super.flush();
                if (output.length() > 0)
                    logHandler.addLog(new Log("AlgorithmOutput", output));
                output = "";
            }
        });
        pythonInterpreters.get(pythonInterpreters.size() - 1).execfile(new FileInputStream(algorithmFile));

        pythonInterpreters.get(pythonInterpreters.size() - 1).exec("init()");
    }

    public void runSubAlgorithm(String path) {
        try {
            this.addPythonInterpreterToStack(new File(path));
            logHandler.addLog(new Log("Algorithm", "Added subAlgorithm: " + path));
        } catch (Exception e) {
            logHandler.addLog(new Log("Algorithm", "Error while opening subAlgorithm"));
            e.printStackTrace();

            this.hardware.setHardwareSafe();
        }
    }

    public void setLogHandler(ILogHandler logHandler) {
        this.logHandler = logHandler;
    }

    public void setHardware(IHardware hardware) {
        this.hardware = hardware;
    }

    public void tick(int tickPeriod) {
        try {
            if (this.pythonInterpreters.size() > 0 && !paused)
                // Tick the top of the stack
                this.pythonInterpreters.get(this.pythonInterpreters.size() - 1).exec("tick(" + (tickPeriod / 1000.0) + ")");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void algorithmDone() {
        if (pythonInterpreters.size() >= 1) {
            pythonInterpreters.remove(pythonInterpreters.size() - 1);
            if (pythonInterpreters.size() >= 1)
                logHandler.addLog(new Log("Algorithm", "SubAlgorithm finished!"));
        }

        if (pythonInterpreters.size() == 0) {
            paused = false;
            runButton.setText("Run");

            logHandler.addLog(new Log("Algorithm", "Algorithm finished!"));
            this.hardware.setHardwareSafe();
        }
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        panel1 = new JPanel();
        panel1.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(4, 3, new Insets(0, 0, 0, 0), -1, -1));
        runButton = new JButton();
        runButton.setText("Run");
        panel1.add(runButton, new com.intellij.uiDesigner.core.GridConstraints(2, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        fileNameLabel = new JLabel();
        fileNameLabel.setText("<No file selected>");
        panel1.add(fileNameLabel, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 2, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        fileSelectButton = new JButton();
        fileSelectButton.setText("...");
        panel1.add(fileSelectButton, new com.intellij.uiDesigner.core.GridConstraints(0, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final com.intellij.uiDesigner.core.Spacer spacer1 = new com.intellij.uiDesigner.core.Spacer();
        panel1.add(spacer1, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 2, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        stopButton = new JButton();
        stopButton.setText("Stop");
        panel1.add(stopButton, new com.intellij.uiDesigner.core.GridConstraints(2, 1, 1, 2, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final com.intellij.uiDesigner.core.Spacer spacer2 = new com.intellij.uiDesigner.core.Spacer();
        panel1.add(spacer2, new com.intellij.uiDesigner.core.GridConstraints(3, 1, 1, 2, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        final com.intellij.uiDesigner.core.Spacer spacer3 = new com.intellij.uiDesigner.core.Spacer();
        panel1.add(spacer3, new com.intellij.uiDesigner.core.GridConstraints(3, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return panel1;
    }
}
