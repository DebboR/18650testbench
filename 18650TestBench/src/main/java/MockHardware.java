import java.util.ArrayList;

public class MockHardware extends IHardware {
    private static boolean VERBOSE = false;

    private ILogHandler logHandler;

    private boolean[] cellCharging = new boolean[8];
    private boolean loadConnected = false;
    private double loadCurrent = 0;
    private boolean chargeConnected = false;

    @Override
    public void init(ILogHandler logHandler) {
        this.logHandler = logHandler;
        if(VERBOSE)
            this.logHandler.addLog(new Log("MockHardware", "Init called"));
    }

    @Override
    public void performMeasurements() {
        this.loadCurrent = -1*Math.random();
        super.performMeasurements();
    }

    @Override
    public double measureCellVoltage(int cellNumber) {
        if(VERBOSE)
            this.logHandler.addLog(new Log("MockHardware", "Getting cell voltage " + cellNumber));
        return ((1.2*Math.random()) + 3.0);
    }

    @Override
    public double measureCellCurrent(int cellNumber) {
        if(VERBOSE)
            this.logHandler.addLog(new Log("MockHardware", "Getting cell current " + cellNumber));

        boolean anyCellCharging = false;
        for(int i=0; i<NUMBER_OF_CELLS; i++) anyCellCharging |= cellCharging[i];

        return (this.cellCharging[cellNumber] ? Math.random() : (this.loadConnected && !anyCellCharging ? this.loadCurrent : 0));
    }

    @Override
    public double measureChargeVoltage() {
        if(VERBOSE)
            this.logHandler.addLog(new Log("MockHardware", "Getting charge voltage"));
        return ((1.2*Math.random()) + 3.0);
    }

    @Override
    public boolean getCellCharging(int cellNumber) {
        if(VERBOSE)
            this.logHandler.addLog(new Log("MockHardware", "Getting cell charging " + cellNumber));
        return this.cellCharging[cellNumber];
    }

    @Override
    public void setCellCharging(int cellNumber, boolean charging) {
        if(VERBOSE)
            this.logHandler.addLog(new Log("MockHardware", "Setting cell charging " + cellNumber));
        this.cellCharging[cellNumber] = charging;
    }

    @Override
    public boolean getLoadConnected() {
        if(VERBOSE)
            this.logHandler.addLog(new Log("MockHardware", "Getting load connected"));
        return this.loadConnected;
    }

    @Override
    public void setLoadConnected(boolean connected) {
        if(VERBOSE)
            this.logHandler.addLog(new Log("MockHardware", "Setting load connected"));
        this.loadConnected = connected;
    }

    @Override
    public boolean getChargeConnected() {
        if(VERBOSE)
            this.logHandler.addLog(new Log("MockHardware", "Getting charge connected"));
        return this.chargeConnected;
    }

    @Override
    public void setChargeConnected(boolean connected) {
        if(VERBOSE)
            this.logHandler.addLog(new Log("MockHardware", "Setting charge connected"));
        this.chargeConnected = connected;
    }
}
