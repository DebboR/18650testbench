import com.pi4j.io.gpio.*;
import com.pi4j.io.i2c.*;

import java.util.HashMap;

public class TestBenchHardware extends IHardware {
    private static final int I2CBUS = I2CBus.BUS_1;
    private static final byte PCF8574_ADDR = 0x20;
    private static final byte[] PCF8574_RELAY_PINS = {0, 1, 2, 3, 4, 5, 6, 7};
    private static final byte[] INA220_ADDRS = {0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47};
    private static final Pin LOAD_RELAY_GPIO_PIN = RaspiPin.GPIO_07;

    private static final int INA_220_CONFIG_ADDR = 0x00;
    private static final int INA_220_SHUNT_VOLTAGE_ADDR = 0x01;
    private static final int INA_220_BUS_VOLTAGE_ADDR = 0x02;

    private static final byte[] INA_220_CONFIG = {(byte) 0b00111110, (byte) 0b11101111};

    private static final double SENSE_RESISTOR = 0.05;

    private ILogHandler logHandler;
    private I2CDevice PCF8574;
    private I2CDevice[] INA220S = new I2CDevice[IHardware.NUMBER_OF_CELLS];
    private boolean[] INA220Online = new boolean[IHardware.NUMBER_OF_CELLS];
    private GpioPinDigitalOutput loadRelayPin;

    private byte chargeRelayState = (byte) 0x00;
    private boolean loadConnected = false;

    @Override
    public void init(ILogHandler logHandler) {
        this.logHandler = logHandler;

        try {
            // Get I²C bus
            I2CBus i2cBus = I2CFactory.getInstance(I2CBUS);

            // Define all I²C devices
            this.PCF8574 = i2cBus.getDevice(PCF8574_ADDR);
            for(int i=0; i<IHardware.NUMBER_OF_CELLS; i++)
                INA220S[i] = i2cBus.getDevice(INA220_ADDRS[i]);

            // Wait for initialization
            Thread.sleep(1500);

            // Set all charge relays to known state
            PCF8574.write(chargeRelayState);

            // Get GPIO controller
            GpioController gpioController = GpioFactory.getInstance();

            // Get Load Relay pin
            loadRelayPin = gpioController.provisionDigitalOutputPin(LOAD_RELAY_GPIO_PIN);

            // Set Load Relay to known state
            loadRelayPin.setState(loadConnected);

        } catch (Exception e) {
            this.logHandler.addLog(new Log("Exception", "Something went wrong while initializing TestBenchHardware"));
            e.printStackTrace();
        }
    }

    @Override
    protected double measureCellVoltage(int cellNumber) {
        try {
            // Read bus voltage register
            byte[] buffer = {0x00, 0x00};
            this.INA220S[cellNumber].read(INA_220_BUS_VOLTAGE_ADDR, buffer, 0, 2);

            // Chip is online!
            if(!this.INA220Online[cellNumber]){
                this.initINA220(cellNumber);
                return 0;
            }

            // Convert to voltage
            int value = (0xFF00 & ((int) buffer[0] << 8)) | (0xFF & ((int) buffer[1]));
            value >>= 3;
            value &= 0x0FFF;
            return (0.004 * value);
        } catch (Exception e){
            // No reaction of chip: OFFLINE
            this.INA220Online[cellNumber] = false;
            return 0;
        }
    }

    @Override
    protected double measureCellCurrent(int cellNumber) {
        try {
            if(this.INA220Online[cellNumber]) {
                // Read shunt voltage register
                byte[] buffer = {0x00, 0x00};
                this.INA220S[cellNumber].read(INA_220_SHUNT_VOLTAGE_ADDR, buffer, 0, 2);

                // Convert to current
                short value = (short) (((short) 0xFF00 & ((short) buffer[0] << 8)) | ((short) 0xFF & ((short) buffer[1])));
                return ((value * 0.00001) / SENSE_RESISTOR);
            }
        } catch (Exception e){
            // No reaction of chip: OFFLINE
            this.INA220Online[cellNumber] = false;
        }
        return 0;
    }

    @Override
    public boolean getCellCharging(int cellNumber) {
        return (this.chargeRelayState & (1 << PCF8574_RELAY_PINS[cellNumber])) != 0;
    }

    @Override
    public void setCellCharging(int cellNumber, boolean charging) {
        if(charging != this.getCellCharging(cellNumber)){
            this.chargeRelayState &= ~(1 << PCF8574_RELAY_PINS[cellNumber]);
            this.chargeRelayState |= ((charging ? 1 : 0) << PCF8574_RELAY_PINS[cellNumber]);

            try {
                this.PCF8574.write(this.chargeRelayState);
            } catch (Exception e){
                this.logHandler.addLog(new Log("Exception", "Something went wrong while setting cell charging on TestBenchHardware"));
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean getLoadConnected() {
        return this.loadConnected;
    }

    @Override
    public void setLoadConnected(boolean connected) {
        this.loadConnected = connected;
        this.loadRelayPin.setState(this.loadConnected);

    }

    @Override
    public double measureChargeVoltage() {
        // Not supported on this hardware!
        return 0;
    }

    @Override
    public boolean getChargeConnected() {
        return true;
    }

    @Override
    public void setChargeConnected(boolean connected) {}

    // Initialize config and calibration of INA220
    private void initINA220(int cellNumber){
        try{
            // Write config
            this.INA220S[cellNumber].write(INA_220_CONFIG_ADDR, INA_220_CONFIG, 0, 2);

            // Set as online
            this.INA220Online[cellNumber] = true;

            this.logHandler.addLog(new Log("TestBench", "Initialized INA220 of cell " + cellNumber));
        } catch(Exception e){}
    }
}
