import JythonInterface

import time

# Constants
CHARGED_CELL_VOLTAGE = 4.3
MAX_DEAD_CELL_VOLTAGE = 3.0
CELL_VOLTAGE_DELTA = 0.1
SETTLING_TIME_S = 60
NUM_CELLS = 8
NUM_TICKS_PER_CALCULATION = 60

# Global variables
settlingStartTime = None
logFile = None
tickCount = NUM_TICKS_PER_CALCULATION

# Overridden methods
def init():
	global logFile
	print("Initializing Charging Algorithm v0.2")
	JythonInterface.setLoadConnected(False)
	logFile = open("chargeLog.csv", "a")

def tick(period):
	global settlingStartTime
	global tickCount
	
	# Log time, cell voltages, currents and charging states
	log()
	
	# Increment tick
	tickCount += 1
	
	if(tickCount >= NUM_TICKS_PER_CALCULATION):
		tickCount = 0
		
		lowestCells = getCellsWithinBounds(getLowestCellVoltage())
		chargedCells = getCellsWithinBounds(CHARGED_CELL_VOLTAGE)
		
		# Set charging switches
		for i in range(NUM_CELLS):
			JythonInterface.setCellCharging(i, ((i in lowestCells) and (i not in chargedCells)))
			
		# Set done if all cells in range
		if(len(chargedCells) == len(getNonDeadCells())):
			# Start timer if not yet started
			if(settlingStartTime == None):
				settlingStartTime = time.time()
				print("Starting settling time")
			# Stop if enough time has elapsed
			if(time.time() - settlingStartTime >= SETTLING_TIME_S):
				done()
		else:
			settlingStartTime = None

def done():
	global logFile
	print("Stopping Charging Algorithm and writing chargeLog.csv")
	logFile.close()
	JythonInterface.done()
	
# Helper methods
def getNonDeadCells():
	nonDeadCells = []
	for i in range(NUM_CELLS):
		if(JythonInterface.getCellVoltage(i) >= MAX_DEAD_CELL_VOLTAGE):
			nonDeadCells.append(i)
	return nonDeadCells
	
def getLowestCellVoltage():
	lowestVoltage = CHARGED_CELL_VOLTAGE
	for i in getNonDeadCells():
		lowestVoltage = min(lowestVoltage, JythonInterface.getCellVoltage(i))
	return lowestVoltage
	
def getCellsWithinBounds(voltage):
	cells = []
	for i in getNonDeadCells():
		cellVoltage = JythonInterface.getCellVoltage(i)
		if(cellVoltage <= (voltage + CELL_VOLTAGE_DELTA) and cellVoltage >= (voltage - CELL_VOLTAGE_DELTA)):
			cells.append(i)
	return cells
	
def log():
	global logFile
	voltages = []
	currents = []
	charging = []
	for i in range(NUM_CELLS):
		voltages.append(JythonInterface.getCellVoltage(i))
		currents.append(JythonInterface.getCellCurrent(i))
		charging.append(1 if JythonInterface.getCellCharging(i) else 0)
	logFile.write(str(time.time()) + ", " + str(voltages)[1:-1] + ", "+ str(currents)[1:-1] + ", "+ str(charging)[1:-1] + "\n")
	
