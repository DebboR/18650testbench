import JythonInterface

import time

# Constants
CHARGED_CELL_VOLTAGE = 4.3
MAX_DEAD_CELL_VOLTAGE = 3.0
CELL_VOLTAGE_DELTA = 0.1
SETTLING_TIME_S = 60
NUM_CELLS = 8
NUM_TICKS_PER_CALCULATION = 60

# Timings
NUM_SETTLING_CALCULATIONS = 60
NUM_DISCHARGE_CALCULATIONS = 20
NUM_CHARGE_CALCULATIONS = 10

CHARGE_ALGO_PATH = "C:\Users\\robbe\OneDrive\VUB\Thesis\TestBench\Code\\18650TestBench\Algorithms\charge.py"

# Global variables
logFile = None
tickCount = NUM_TICKS_PER_CALCULATION
cellUnderTest = 0
calculationCount = 0

# Overridden methods
def init():
    global logFile
    print("Initializing StepResponseCharacterization Algorithm v0.1")
    print("Charging up all cells in advance")
    JythonInterface.runSubAlgorithm(CHARGE_ALGO_PATH)
    JythonInterface.setLoadConnected(False)
    initLogFile()

def tick(period):
    global settlingStartTime
    global tickCount
    global calculationCount

    # Log time, cell voltages, currents and charging states
    log()

    # Increment tick
    tickCount += 1

    # If there is a dead cell -> quit the load!
    if len(getNonDeadCells()) < NUM_CELLS:
        print("Detected dead cell! Quitting the program")
        JythonInterface.setLoadConnected(False)
        done()

    # Perform a calculation if there are enough ticks in the count
    if(tickCount >= NUM_TICKS_PER_CALCULATION):
        tickCount = 0

        # Increment calculation count
        calculationCount += 1

        # Switch case to perform step response measurement
        if calculationCount <= NUM_SETTLING_CALCULATIONS:
            JythonInterface.setLoadConnected(False)
            setOnlyChargingCell(-1)
        elif calculationCount > NUM_SETTLING_CALCULATIONS and calculationCount <= NUM_SETTLING_CALCULATIONS + NUM_DISCHARGE_CALCULATIONS:
            JythonInterface.setLoadConnected(True)
            setOnlyChargingCell(-1)
        elif calculationCount > NUM_SETTLING_CALCULATIONS + NUM_DISCHARGE_CALCULATIONS and calculationCount <= 2*NUM_SETTLING_CALCULATIONS + NUM_DISCHARGE_CALCULATIONS:
            JythonInterface.setLoadConnected(False)
            setOnlyChargingCell(-1)
        elif calculationCount > 2*NUM_SETTLING_CALCULATIONS + NUM_DISCHARGE_CALCULATIONS and calculationCount <= 2*NUM_SETTLING_CALCULATIONS + NUM_CHARGE_CALCULATIONS + NUM_DISCHARGE_CALCULATIONS:
            JythonInterface.setLoadConnected(False)
            setOnlyChargingCell(cellUnderTest)
        elif calculationCount > 2*NUM_SETTLING_CALCULATIONS + NUM_CHARGE_CALCULATIONS + NUM_DISCHARGE_CALCULATIONS and calculationCount <= 3*NUM_SETTLING_CALCULATIONS + NUM_CHARGE_CALCULATIONS + NUM_DISCHARGE_CALCULATIONS:
            JythonInterface.setLoadConnected(False)
            setOnlyChargingCell(-1)
        else:
            switchToNextCell()

def done():
    global logFile
    print("Stopping StepResponseCharacterization Algorithm")
    logFile.close()
    JythonInterface.done()

# Helper methods
def getNonDeadCells():
    nonDeadCells = []
    for i in range(NUM_CELLS):
        if(JythonInterface.getCellVoltage(i) >= MAX_DEAD_CELL_VOLTAGE):
            nonDeadCells.append(i)
    return nonDeadCells

def getLowestCellVoltage():
    lowestVoltage = CHARGED_CELL_VOLTAGE
    for i in getNonDeadCells():
        lowestVoltage = min(lowestVoltage, JythonInterface.getCellVoltage(i))
    return lowestVoltage

def getCellsWithinBounds(voltage):
    cells = []
    for i in getNonDeadCells():
        cellVoltage = JythonInterface.getCellVoltage(i)
        if(cellVoltage <= (voltage + CELL_VOLTAGE_DELTA) and cellVoltage >= (voltage - CELL_VOLTAGE_DELTA)):
            cells.append(i)
    return cells

def switchToNextCell():
    global logFile
    global cellUnderTest
    global calculationCount
    print("Switching to next cell, but charging first!")
    JythonInterface.runSubAlgorithm(CHARGE_ALGO_PATH)
    logFile.close()
    cellUnderTest += 1
    calculationCount = 0
    if cellUnderTest >= NUM_CELLS:
        print("Tested all non-dead cells")
        done()
    initLogFile()

def setOnlyChargingCell(cellNumber):
    for i in getNonDeadCells():
        JythonInterface.setCellCharging(i, (i == cellNumber))

def initLogFile():
    global logFile
    global cellUnderTest
    fileName = "cellStepLog" + str(cellUnderTest) + ".csv"
    print("Opening log file: " + fileName)
    logFile = open(fileName, "a")


def log():
    global logFile
    voltages = []
    currents = []
    charging = []
    for i in range(NUM_CELLS):
        voltages.append(JythonInterface.getCellVoltage(i))
        currents.append(JythonInterface.getCellCurrent(i))
        charging.append(1 if JythonInterface.getCellCharging(i) else 0)
    logFile.write(str(time.time()) + ", " + str(voltages)[1:-1] + ", "+ str(currents)[1:-1] + ", "+ str(charging)[1:-1] + "\n")

