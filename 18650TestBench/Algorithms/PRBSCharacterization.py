import JythonInterface

import time

# Constants
CHARGED_CELL_VOLTAGE = 4.3
LOW_CELL_VOLTAGE = 3.1
MAX_DEAD_CELL_VOLTAGE = 3.0
CELL_VOLTAGE_DELTA = 0.1
SETTLING_TIME_S = 60
NUM_CELLS = 8
NUM_TICKS_PER_CALCULATION = 2*60

CHARGE_ALGO_PATH = "C:\Users\\robbe\OneDrive\VUB\Thesis\TestBench\Code\\18650TestBench\Algorithms\charge.py"
PRBS = [False, False, False, False, False, False, False, False, True, False, False, True, False, False, True, True, False, False, False, False, True, True, True, False, False, True, False, True, False, False, False, True, True, False, True, True, True, True, False, False, False, False, True, False, False, False, True, False, False, False, False, True, False, True, True, True, True, True, False, True, True, True, True, True, True, False, True, False, False, True, False, False, False, False, False, True, True, False, False, True, False, False, False, True, True, True, False, True, False, False, False, True, False, True, True, False, False, True, True, False, True, False, True, False, True, False, True, True, False, True, True, False, True, True, True, False, True, False, True, True, True, True, False, True, True, False, False, True, False, True, True, True, False, False, False, False, False, True, False, True, False, False, True, False, True, True, False, True, False, True, True, False, False, False, False, False, False, True, True, True, True, True, True, True, False, False, True, False, False, True, False, True, False, True, True, True, False, True, True, False, True, False, False, False, False, True, True, False, True, False, False, True, True, True, False, True, True, True, False, False, True, True, False, False, True, True, True, False, False, False, True, True, True, True, False, False, True, True, True, True, True, False, False, False, True, False, False, True, True, True, True, False, True, False, True, False, False, True, True, False, True, True, False, False, False, True, True, False, False, False, True, False, True, False, True]

# Global variables
logFile = None
tickCount = NUM_TICKS_PER_CALCULATION
prbsIndex = 0
doneOnNextTick = False

# Overridden methods
def init():
    global logFile
    print("Initializing PRBSCharacterization Algorithm v0.1")
    print("Charging up all cells in advance")
    JythonInterface.runSubAlgorithm(CHARGE_ALGO_PATH)
    JythonInterface.setLoadConnected(False)
    initLogFile()

def tick(period):
    global tickCount
    global prbsIndex
    global doneOnNextTick

    # Log time, cell voltages, currents and charging states
    log()

    # Increment tick
    tickCount += 1

    # If there is a dead cell -> quit the load!
    if len(getNonDeadCells()) < NUM_CELLS:
        print("Detected dead cell! Quitting the program")
        JythonInterface.setLoadConnected(False)
        doneOnNextTick = True

    # If there is a low cell -> quit the load!
    if len(getNonLowCells()) < NUM_CELLS:
        print("Detected low cell! Quitting the program after charging")
        JythonInterface.setLoadConnected(False)
        JythonInterface.runSubAlgorithm(CHARGE_ALGO_PATH)
        doneOnNextTick = True

    # If done in previous tick, quit!
    if doneOnNextTick:
        done()

    # Perform a calculation if there are enough ticks in the count
    if(tickCount >= NUM_TICKS_PER_CALCULATION):
        tickCount = 0

        if(PRBS[prbsIndex]):
            JythonInterface.setLoadConnected(False)
            for i in range(NUM_CELLS):
                JythonInterface.setCellCharging(i, True)
        else:
            JythonInterface.setLoadConnected(True)
            for i in range(NUM_CELLS):
                JythonInterface.setCellCharging(i, False)

        prbsIndex += 1
        prbsIndex %= len(PRBS)


def done():
    global logFile
    print("Stopping PRBSCharacterization Algorithm")
    logFile.close()
    JythonInterface.done()

# Helper methods
def getNonDeadCells():
    nonDeadCells = []
    for i in range(NUM_CELLS):
        if(JythonInterface.getCellVoltage(i) >= MAX_DEAD_CELL_VOLTAGE):
            nonDeadCells.append(i)
    return nonDeadCells

def getNonLowCells():
    nonLowCells = []
    for i in range(NUM_CELLS):
        if(JythonInterface.getCellVoltage(i) >= LOW_CELL_VOLTAGE):
            nonLowCells.append(i)
    return nonLowCells

def getLowestCellVoltage():
    lowestVoltage = CHARGED_CELL_VOLTAGE
    for i in getNonDeadCells():
        lowestVoltage = min(lowestVoltage, JythonInterface.getCellVoltage(i))
    return lowestVoltage

def getCellsWithinBounds(voltage):
    cells = []
    for i in getNonDeadCells():
        cellVoltage = JythonInterface.getCellVoltage(i)
        if(cellVoltage <= (voltage + CELL_VOLTAGE_DELTA) and cellVoltage >= (voltage - CELL_VOLTAGE_DELTA)):
            cells.append(i)
    return cells


def initLogFile():
    global logFile
    fileName = "PRBSLog.csv"
    print("Opening log file: " + fileName)
    logFile = open(fileName, "a")


def log():
    global logFile
    voltages = []
    currents = []
    charging = []
    for i in range(NUM_CELLS):
        voltages.append(JythonInterface.getCellVoltage(i))
        currents.append(JythonInterface.getCellCurrent(i))
        charging.append(1 if JythonInterface.getCellCharging(i) else 0)
    logFile.write(str(time.time()) + ", " + str(voltages)[1:-1] + ", "+ str(currents)[1:-1] + ", "+ str(charging)[1:-1] + "\n")

