import JythonInterface

tickCount = 0

# Overridden methods
def init():
    global logFile
    print("Initializing MasterTest Algorithm v0.1")
    JythonInterface.setLoadConnected(False)

def tick(period):
    global tickCount
    tickCount += 1
    if tickCount == 1:
        JythonInterface.runSubAlgorithm("C:\Users\\robbe\OneDrive\VUB\Thesis\TestBench\Code\\18650TestBench\Algorithms\SlaveTest.py")
    else:
        done()

def done():
    global logFile
    print("Stopping MasterTest Algorithm")
    JythonInterface.done()
