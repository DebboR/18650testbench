import JythonInterface

import time

# Constants
CHARGED_CELL_VOLTAGE = 4.2
EMPTY_CELL_VOLTAGE = 3.2
MAX_DEAD_CELL_VOLTAGE = 3.0
CELL_VOLTAGE_DELTA = 0.1
NUM_CELLS = 8
NUM_TICKS_PER_CALCULATION = 60

# Global variables
logFile = None
tickCount = NUM_TICKS_PER_CALCULATION
cellUnderTest = 0
cellCapacity = 0
chargingCell = None

# Overridden methods
def init():
    global logFile
    global chargingCell
    global cellUnderTest
    print("Initializing Capacity Measurement Algorithm v0.1")
    JythonInterface.setLoadConnected(False)
    logFile = open("capacityLog.csv", "a")

    # Start by charging the 1st cell
    chargingCell = cellUnderTest

def tick(period):
    global tickCount
    global cellUnderTest
    global chargingCell
    global cellCapacity

    # Log time, cell voltages, currents and charging states
    log()

    # If there is a dead cell -> quit the load!
    if len(getNonDeadCells()) < NUM_CELLS:
        print("Detected dead cell! Quitting the program")
        JythonInterface.setLoadConnected(False)
        done()

    # Add to cell capacity
    tempCurrent = JythonInterface.getCellCurrent(cellUnderTest)
    if tempCurrent < 0:
        cellCapacity += (-1000 * tempCurrent * period / 3600)

    # Increment tick
    tickCount += 1

    # Check if cell not dead, otherwise switch to next cell!
    if cellUnderTest not in getNonDeadCells():
        print("Cell under test " + str(cellUnderTest) + " is dead. Moving to next cell")
        cellUnderTest += 1
        if cellUnderTest >= NUM_CELLS:
            print("Tested all non-dead cells")
            done()

    if tickCount >= NUM_TICKS_PER_CALCULATION:
        tickCount = 0

        # If charging other cell
        if chargingCell != None:
            JythonInterface.setLoadConnected(False)
            for i in range(NUM_CELLS):
                JythonInterface.setCellCharging(i, True if i == chargingCell else False)
            # If cell charged, quit charging and continue
            if chargingCell in getCellsWithinBounds(CHARGED_CELL_VOLTAGE):
                chargingCell = None

        # If not charging other cell
        else:
            # Enable load and disable charging
            JythonInterface.setLoadConnected(True)
            for i in range(NUM_CELLS):
                JythonInterface.setCellCharging(i, False)

            # Check if there are cells in need of charging
            cellsToCharge = getCellsWithinBounds(EMPTY_CELL_VOLTAGE)
            if cellUnderTest in cellsToCharge:
                # Cell is empty, charge it and move to next cell
                print ("Done testing cell " + str(cellUnderTest) + ". Capacity: " + str(cellCapacity) + "mAh")
                cellUnderTest += 1
                cellCapacity = 0
                chargingCell = cellUnderTest
            elif len(cellsToCharge) > 0:
                chargingCell = cellsToCharge[0]


def done():
    global logFile
    print("Stopping Capacity Measurement Algorithm and writing capacityLog.csv")
    JythonInterface.setLoadConnected(False)
    logFile.close()
    JythonInterface.done()

# Helper methods
def getNonDeadCells():
    nonDeadCells = []
    for i in range(NUM_CELLS):
        if JythonInterface.getCellVoltage(i) >= MAX_DEAD_CELL_VOLTAGE:
            nonDeadCells.append(i)
    return nonDeadCells

def getLowestCellVoltage():
    lowestVoltage = CHARGED_CELL_VOLTAGE
    for i in getNonDeadCells():
        lowestVoltage = min(lowestVoltage, JythonInterface.getCellVoltage(i))
    return lowestVoltage

def getCellsWithinBounds(voltage):
    cells = []
    for i in getNonDeadCells():
        cellVoltage = JythonInterface.getCellVoltage(i)
        if cellVoltage <= (voltage + CELL_VOLTAGE_DELTA) and cellVoltage >= (voltage - CELL_VOLTAGE_DELTA):
            cells.append(i)
    return cells

def log():
    global logFile
    voltages = []
    currents = []
    charging = []
    for i in range(NUM_CELLS):
        voltages.append(JythonInterface.getCellVoltage(i))
        currents.append(JythonInterface.getCellCurrent(i))
        charging.append(1 if JythonInterface.getCellCharging(i) else 0)
    logFile.write(str(time.time()) + ", " + str(voltages)[1:-1] + ", "+ str(currents)[1:-1] + ", "+ str(charging)[1:-1] + "\n")

