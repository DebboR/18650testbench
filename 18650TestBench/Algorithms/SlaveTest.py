import JythonInterface

tickCount = 0

# Overridden methods
def init():
    global logFile
    print("Initializing SlaveTest Algorithm v0.1")
    JythonInterface.setLoadConnected(False)

def tick(period):
    global tickCount

    tickCount += 1

    if tickCount > 5:
        done()

def done():
    global logFile
    print("Stopping SlaveTest Algorithm")
    JythonInterface.done()